import flask
from flask import Flask, request
import random
import pymongo
from bson import ObjectId
import json
import datetime
from flask_cors import CORS
import nltk
from nltk import sentiment
from textblob import TextBlob

app = Flask(__name__)
CORS(app)
class JSONEncoder(json.JSONEncoder):
    ''' extend json-encoder class'''
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime.datetime):
            return str(o)
        return json.JSONEncoder.default(self, o)

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["users"]
mycoaches = mydb["coach"]
myquery = { "name": "Tamara Jovic" }

@app.route('/')
def hello_world():
    print('hello')
    return 'Hello, World!'

@app.route('/coaches', methods=['GET'])
def get_coaches():
    docs = list(mycoaches.find(myquery))
    return  JSONEncoder().encode(docs), 200

@app.route('/coaches/one', methods=['PUT'])
def add_review():
    data = dict(request.get_json())
    value = []
    for com in data["reviews"]:
        sent = TextBlob(com)
        s = sent.sentiment
        value.append(s.polarity)
    avg_opinion = sum(value)/len(value) # Prosecna vrednost komentara (1=pozitivno, 0=neutralno, -1=negativno)
    mycoaches.update_many({ "_id": ObjectId(data['_id']) }, {"$set": {'reviews': data["reviews"], 'opinion': avg_opinion} })
    return ""

app.run("", 8000, threaded=True)