'''
    Review Generator, Serbian Names Generator
'''
import random
import pymongo
from bson import ObjectId
import nltk
from nltk import sentiment
from textblob import TextBlob
from string import digits
import time
# coding: utf8

names = ['Milica', 'Andela', 'Jovana', 'Ana', 'Teodora', 'Sara', 'Katarina', 'Anastasija', 'Aleksandra', 'Kristina', 'Nikolina', 'Marija', 'Nina', 'Tamara', 'Jelena', 'Ivana', 'Sofija', 'Tijana', 'Nevena', 'Natalija', 'Luka', 'Nikola', 'Stefan', 'Marko', 'Lazar', 'Aleksandar', 'Aleksa', 'Filip', 'Nemanja', 'Jovan', 'Miloš', 'Ognjen', 'Uroš', 'Petar', 'Đorđe', 'Dušan', 'Mihajlo', 'Milan', 'Veljko', 'Vuk']
surnames = ['Jovanovic', 'Petrovic', 'Nikolic', 'Ilic', 'Đorđevic', 'Pavlovic', 'Markovic', 'Popovic', 'Stojanovic', 'Živkovic', 'Jankovic', 'Todorovic', 'Stankovic', 'Ristic', 'Kostic', 'Miloševic', 'Cvetkovic', 'Kovačevic', 'Dimitrijevic', 'Tomic', 'Krstic', 'Ivanovic', 'Lukic', 'Filipovic', 'Savic', 'Mitrovic', 'Lazic', 'Petkovic', 'Obradovic', 'Aleksic', 'Radovanovic', 'Lazarevic', 'Vasic', 'Milovanovic', 'Jovic', 'Stevanovic', 'Milenkovic', 'Milosavljevic', 'Mladenovic', 'Živanovic', 'Simic', 'Đuric', 'Nedeljkovic', 'Novakovic', 'Marinkovic', 'Bogdanovic', 'Kneževic', 'Radosavljevic', 'Mihajlovic', 'Gajic', 'Mitic', 'Stefanovic', 'Blagojevic', 'Antic', 'Vasiljevic', 'Jevtic', 'Đokic', 'Stojkovic', 'Vukovic', 'Rakic', 'Stanojevic', 'Pešic', 'Tasic', 'Milic', 'Milanovic', 'Zdravkovic', 'Grujic', 'Babic', 'Vučkovic', 'Matic', 'Peric', 'ciric', 'Paunovic', 'Marjanovic', 'Maksimovic', 'Anđelkovic', 'Jakovljevic', 'Gavrilovic', 'Veljkovic', 'Tošic', ' Trajkovic', 'Ivkovic', 'Arsic', 'Miletic', 'Veličkovic', 'Radovic', 'Miljkovic', 'Nešic', 'Jeremic', 'Radulovic', 'Đurđevic', 'Milojevic', 'Uroševic', 'Boškovic', 'Trifunovic', 'Božic', 'Radivojevic', 'Đukic', 'Milutinovic', 'Stamenkovic']
hobbies = ['Basketball', 'Footbal', 'Volleyball', 'Running', 'Dancing', 'Reading', 'Hiking', 'Skiing', 'Surfing', 'Horse Riding']
jobs = ["Accountant", "Teacher", "Developer", "Manager", "HR", "Sales", "Librarian", "Doctor", "Farmer", "Psycologist", "Dentist"]
country = ['Serbia']
city = ['Novi Sad', 'Belgrade', 'Nis', 'Subotica', 'Sombor', 'Valjevo', 'Kraljevo', 'Kikinda', 'Pejicevi Salasi']
birth = [1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1958, 1959, 1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981, 1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009]
korisnici = []

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["users"]
mycol = mydb["coach"]

# print(myclient.list_database_names())
# print(mydb.list_collection_names())

tic = time.perf_counter()
for i in range(1000000):
    user = {}
    user["name"] = random.choice(names) + " " + random.choice(surnames)
    user["hobby"] = random.choice(hobbies)
    user["country"] = random.choice(country)
    user["city"] = random.choice(city)
    user["job"] = random.choice(jobs)
    user["birth"] = random.choice(birth)
    # mycol.insert_one(user)
    korisnici.append(user)

mycol.insert_many(korisnici)
print(mycol.count_documents({}))
# mydb.coaches.delete_many({})
# mycol.create_index([('birth', pymongo.TEXT)], name='search_index', default_language='english')
mycol.create_index([("birth", pymongo.ASCENDING)])
# mycol.ensure_index({ "birth": 1 })
toc = time.perf_counter()
print(f"Users inserted in: {toc - tic:0.4f} seconds")

tic = time.perf_counter()

mycol.find({ "birth": 1998 })

toc = time.perf_counter()
print(f"Users found in: {toc - tic:0.4f} seconds")

first_neg = []
second_neg = []
third_neg = []

positive_comment_list = [
        ["this ", "the ", "my "],
        ["pt ", "coach ", "online coach ", "personal trainer ", "personal coach "],
        ["is ", "looks ", "trains ", "is really "],
        [
            "great",
            "super",
            "good",
            "very good",
            "good",
            "JUST wow",
            "..like.. WOW",
            "cool",
            "GREAT",
            "magnificent",
            "magical",
            "very cool",
            "stylish",
            "beautiful",
            "so beautiful",
            "so stylish",
            "so professional",
            "lovely",
            "so lovely",
            "very lovely",
            "professionally",
            "humble",
            "glorious",
            "so glorious",
            "very glorious",
            "adorable",
            "excellent",
            "amazing",
        ],
        [".", "..", "...", "!", "!!", "!!!"]
    ]
negtive_comment_list = [
        ["this ", "the ", "my "],
        ["pt ", "coach ", "online coach ", "personal trainer ", "personal coach "],
        ["is "],
        ["arrogant", "self-centred", "vain", "boastful", "pompus", "cynical", "overcritical", "bossy", "cruel", "impolite"],
        [".", "..", "...", "!", "!!", "!!!"]
]

neutral_comment_list = [
        ["this ", "the ", "my "],
        ["pt ", "coach ", "online coach ", "personal trainer ", "personal coach "],
        ["is "],
        ["fine", "ok", "okay", "descent", "right"],
        [".", "..", "..."]
]

def text_to_words(text):
    '''Transform a string to a list of words,
    removing all punctuation.'''
    text = text.lower()

    p = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'
    text = ''.join([ch for ch in text if ch not in p])

    return text.split()

def generate_comment(comment_list):
    comm = ''
    for lista in comment_list:
        comm += random.choice(lista)
    return comm


# List of comments
pos_comments = []
neg_comments = []
neu_comments = []

counter = 0
while counter < 1000:
    pos_comments.append(generate_comment(positive_comment_list))
    counter += 1

counter = 0
while counter < 1000:
    neg_comments.append(generate_comment(negtive_comment_list))
    counter += 1

counter = 0
while counter < 1000:
    neu_comments.append(generate_comment(neutral_comment_list))
    counter += 1

def randomise_comments():
    comments = []
    for i in range(random.randint(5, random.randint(10, 20))):
        comments.append(random.choice(pos_comments))
    for i in range(random.randint(5, random.randint(10, 20))):
        comments.append(random.choice(neu_comments))
    for i in range(random.randint(5, random.randint(10, 20))):
        comments.append(random.choice(neg_comments))
    return comments

'''
    MongoDB connection
'''
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["users"]
mycoaches = mydb["coach"]

def appendComments():
    for doc in mycoaches.find():
        comments = randomise_comments()
        value = []
        for com in comments:
                sent = TextBlob(com)
                s = sent.sentiment
                value.append(s.polarity)
        avg_opinion = sum(value)/len(value) # Prosecna vrednost komentara (1=pozitivno, 0=neutralno, -1=negativno)
        mycoaches.update({"_id": ObjectId(doc['_id'])},
                        {'$set' : {'reviews' : comments, 'opinion': avg_opinion }})
    print("Commments appended !")

# appendComments()
# myquery = { "name": "Tamara Petkovic" }
# for doc in mycoaches.find():
#     print(len(doc["reviews"]))
#     break

# docs = mycoaches.find({ "name": "Tamara Petkovic" })
# for coach in docs:
#     print(coach["reviews"], "\n")

# docs = mycoaches.find(myquery)
# for i in docs:
#     print(i)