import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getCoaches() {
    console.log('new');
    return this.httpClient.get<any[]>(`http://localhost:8000/coaches`);
  }

  save(coach) {
    return this.httpClient.put<any[]>(`http://localhost:8000/coaches/one`, coach);
  }

}
