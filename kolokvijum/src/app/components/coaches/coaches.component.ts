import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-coaches',
  templateUrl: './coaches.component.html',
  styleUrls: ['./coaches.component.css']
})
export class CoachesComponent implements OnInit {
  coaches;
  coach;
  constructor(
    private api: ApiService
  ) { }

  ngOnInit() {
    this.api.getCoaches().subscribe((data) => {
      this.coaches = data;
    });
  }

  selectReviews(reviews) {
    this.coach = reviews;
  }

  onVoted($event) {
    this.coach = null;
    this.ngOnInit();
  }

}
