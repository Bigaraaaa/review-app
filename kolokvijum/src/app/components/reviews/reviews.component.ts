import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReviewsComponent {
  @Input() coach;
  @Output() voted = new EventEmitter();
  review;

  constructor(
    private apiService: ApiService
  ) { }

  save() {
    this.coach.reviews.push(this.review);
    this.apiService.save(this.coach).subscribe(_ => {
      this.voted.emit(this.coach._id);
    });
    this.review = '';
  }
}
