import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoachesComponent } from './components/coaches/coaches.component';


const routes: Routes = [
  { path: 'coaches', component: CoachesComponent },
  { path: '**', redirectTo: '/coaches', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
